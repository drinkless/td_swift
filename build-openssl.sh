#!/bin/sh
git clone https://github.com/pybee/Python-Apple-support
#TODO: change openssl version
platforms="macOS iOS watchOS tvOS"
#platforms="macOS"
for platform in $platforms;
do
echo $platform
cd Python-Apple-support
#NB: -j will fail
make OpenSSL.framework-$platform
cd ..
rm -rf third_party/openssl/$platform
mkdir -p third_party/openssl/$platform/lib
cp ./Python-Apple-support/build/$platform/libcrypto.a third_party/openssl/$platform/lib/
cp -r ./Python-Apple-support/build/macOS/OpenSSL.framework/Versions/Current/Headers/ third_party/openssl/$platform/include
done
