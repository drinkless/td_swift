#/bin/sh
rm -rf build
mkdir -p build
cd build

platforms="macOS iOS watchOS"
for platform in $platforms;
do
  echo "Platform = ${platform} Simulator = ${simulator}"
  openssl_path=$(realpath ../third_party/openssl/${platform})
  echo $openssl_path
  options="$options -DOPENSSL_INCLUDE_DIR=${openssl_path}/include"
  options="$options -DOPENSSL_LIBRARIES=${openssl_path}/lib/libcrypto.a"
  options="$options -DCMAKE_BUILD_TYPE=Release"
  if [[ $platform = "macOS" ]]; then
    build="build-${platform}"
    rm -rf $build
    mkdir -p $build
    cd $build
    cmake ../../td $options -GNinja
    ninja tdclientjson || exit
    cd ..
    mkdir -p $platform
    cp $build/libtdclientjson.dylib $platform/libtd.dylib
    install_name_tool -id @rpath/libtd.dylib $platform/libtd.dylib
  else
    simulators="0 1"
    for simulator in $simulators;
    do
      build="build-${platform}"
      if [[ $simulator = "1" ]]; then
        build="${build}-simulator"
	ios_platform="SIMULATOR"
      else
        ios_platform="OS"
      fi
      if [[ $platform = "watchOS" ]]; then
	ios_platform="WATCH${ios_platform}"
      fi
      echo $ios_platform
      rm -rf $build
      mkdir -p $build
      cd $build
      cmake ../../td $options -GNinja -DIOS_PLATFORM=${ios_platform} -DCMAKE_TOOLCHAIN_FILE=../td/CMake/iOS.cmake
      ninja -v tdclientjson || exit
      cd ..
    done
    lib="build-${platform}/libtdclientjson.dylib"
    lib_simulator="build-${platform}-simulator/libtdclientjson.dylib"
    mkdir -p $platform
    lipo -create $lib $lib_simulator -o $platform/libtd.dylib
    install_name_tool -id @rpath/libtd.dylib $platform/libtd.dylib
   fi

   mkdir -p ../tdswift/libs/$platform
   cp $platform/libtd.dylib ../tdswift/libs/$platform/
done
rsync ../td/td/telegram/td_json_client.h ../tdswift/Sources/td_json_client.h
