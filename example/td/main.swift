//
//  main.swift
//  td
//
//  Created by Arseny Smirnov  on 12/05/2017.
//  Copyright © 2017 Arseny Smirnov . All rights reserved.
//

import Foundation

class TdClient {
    typealias Client = UnsafeMutableRawPointer
    var client = td_json_client_create(0, 12183, "41c3080d9028cf002792a512d4e20089", "en", "Desktop" , "unknown" , "td-swift" , 1, ".", ".")!
    let tdlibMainLoop = DispatchQueue(label: "tdlib")
    let tdlibQueryQueue = DispatchQueue(label: "tdlibQuery")
    var queryF = Dictionary<Int64, (Dictionary<String,Any>)->()>()
    var updateF : (Dictionary<String,Any>)->()
    var queryId : Int64 = 0
    var closing = false
    
    func queryAsync(query: [String: Any], f: @escaping (Dictionary<String,Any>)->()) {
        tdlibQueryQueue.async {
            var newQuery = query
            let nextQueryId = self.queryId + 1
            newQuery["extra"] = nextQueryId
            self.queryF[nextQueryId] = f
            self.queryId = nextQueryId
            td_json_client_write(self.client, to_json(newQuery))
        }
    }
    
    func querySync(query: [String: Any]) -> Dictionary<String,Any> {
        let semaphore = DispatchSemaphore(value:0)
        var result = Dictionary<String,Any>()
        queryAsync(query: query) {
            result = $0
            semaphore.signal()
        }
        semaphore.wait()
        return result
        
    }
    
    func close() {
        closing = true
        td_json_client_wake_up(client)
    }
    
    deinit {
        td_json_client_destroy(client)
    }
    init(updateHandler: @escaping (Dictionary<String,Any>)->()) {
        updateF = updateHandler
        tdlibMainLoop.async { [weak self] in
            while (true) {
                if let s = self {
                    if s.closing {
                        break
                    }
                    if let res = td_json_client_read(s.client,10) {
                        let event = String(cString: res)
                        s.queryResultAsync(event)
                    }
                } else {
                    break
                }
            }
        }
    }
    
    private func queryResultAsync(_ result: String) {
        tdlibQueryQueue.async {
            let json = try? JSONSerialization.jsonObject(with: result.data(using: .utf8)!, options:[])
            if let dictionary =  json as? [String:Any] {
                if let extra = dictionary["extra"] as? Int64 {
                    let index = self.queryF.index(forKey: extra)!
                    self.queryF[index].value(dictionary)
                    self.queryF.remove(at: index)
                } else {
                    self.updateF(dictionary)
                }
            }
        }
    }
    

}

// Just an example of usage
func to_json(_ obj: Any)  -> String  {
    do {
        let obj = try JSONSerialization.data(withJSONObject: obj)
        return String(data: obj, encoding: .utf8)!
    } catch {
        return ""
    }
    
}
func myReadLine() -> String {
    while (true) {
        if let line = readLine() {
            return line
        }
    }
}


var client = TdClient() {
    let update = $0
    print(update)
}
var authState = client.querySync(query:["_":"getAuthState"])
var authLoop = true
while (authLoop) {
    switch(authState["_"] as! String) {
    case "authStateWaitPhoneNumber":
        print("Enter your phone: ")
        let phone = myReadLine()
        authState = client.querySync(query:["_":"setAuthPhoneNumber", "phone_number":phone])
        
    case "authStateWaitCode":
        var first_name : String = ""
        var last_name: String = ""
        var code : String = ""
        if let is_registered = authState["is_registered"] as? Bool {
        } else {
            print("Enter your first name : ")
            first_name = myReadLine()
            print("Enter your last name: ")
            last_name = myReadLine()
        }
        print("Enter (sms) code: ")
        code = myReadLine()
        authState = client.querySync(query:["_":"checkAuthCode", "code":code, "first_name":first_name, "last_name":last_name])
    case "authStateWaitPassword":
        print("Enter password: ")
        let password = myReadLine()
        authState = client.querySync(query:["_":"checkAuthPassword", "password":password])
    case "authStateOk":
        authLoop = false
    case "error":
        authState = client.querySync(query:["_":"getAuthState"])
    default:
        assert(false, "TODO: Unknown auth state ");
        
    }
}

// authorized after this point


