//
//  File.swift
//  tdswift
//
//  Created by Arseny Smirnov  on 23/05/2017.
//
//

import Foundation

public func f()->Int32 {
    return td_json_client_square(123, "Hello")
}

public func to_json(_ obj: Any)  -> String  {
    do {
        let obj = try JSONSerialization.data(withJSONObject: obj)
        return String(data: obj, encoding: .utf8)!
    } catch {
        return ""
    }
    
}

public class TdClient {
    typealias Client = UnsafeMutableRawPointer
    var client = td_json_client_create(0, 12183, "41c3080d9028cf002792a512d4e20089", "en", "Desktop" , "unknown" , "td-swift" , 1, ".", ".")!
    let tdlibMainLoop = DispatchQueue(label: "tdlib")
    let tdlibQueryQueue = DispatchQueue(label: "tdlibQuery")
    var queryF = Dictionary<Int64, (Dictionary<String,Any>)->()>()
    var updateF : (Dictionary<String,Any>)->()
    var queryId : Int64 = 0
    var closing = false
    
    public func queryAsync(query: [String: Any], f: @escaping (Dictionary<String,Any>)->()) {
        tdlibQueryQueue.async {
            var newQuery = query
            let nextQueryId = self.queryId + 1
            newQuery["extra"] = nextQueryId
            self.queryF[nextQueryId] = f
            self.queryId = nextQueryId
            td_json_client_write(self.client, to_json(newQuery))
        }
    }
    
    public func querySync(query: [String: Any]) -> Dictionary<String,Any> {
        let semaphore = DispatchSemaphore(value:0)
        var result = Dictionary<String,Any>()
        queryAsync(query: query) {
            result = $0
            semaphore.signal()
        }
        semaphore.wait()
        return result
        
    }
    
    public func close() {
        closing = true
        td_json_client_wake_up(client)
    }
    
    deinit {
        td_json_client_destroy(client)
    }
    public init(updateHandler: @escaping (Dictionary<String,Any>)->()) {
        updateF = updateHandler
        tdlibMainLoop.async { [weak self] in
            while (true) {
                if let s = self {
                    if s.closing {
                        break
                    }
                    if let res = td_json_client_read(s.client,10) {
                        let event = String(cString: res)
                        s.queryResultAsync(event)
                    }
                } else {
                    break
                }
            }
        }
    }
    
    private func queryResultAsync(_ result: String) {
        tdlibQueryQueue.async {
            let json = try? JSONSerialization.jsonObject(with: result.data(using: .utf8)!, options:[])
            if let dictionary =  json as? [String:Any] {
                if let extra = dictionary["extra"] as? Int64 {
                    let index = self.queryF.index(forKey: extra)!
                    self.queryF[index].value(dictionary)
                    self.queryF.remove(at: index)
                } else {
                    self.updateF(dictionary)
                }
            }
        }
    }
    

}
