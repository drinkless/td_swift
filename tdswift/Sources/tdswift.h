//
//  tdswift_ios.h
//  tdswift_ios
//
//  Created by Arseny Smirnov  on 23/05/2017.
//
//


#import <Foundation/Foundation.h>

//! Project version number for tdswift.
FOUNDATION_EXPORT double tdswift_VersionNumber;

//! Project version string for tdswift_ios.
FOUNDATION_EXPORT const unsigned char tdswift_VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <tdswift/PublicHeader.h>

#include "td_json_client.h"

