#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int td_json_client_square(int x, const char *str);

void *td_json_client_create(int is_test_dc, int api_id, const char *api_hash, const char *language_code,
                            const char *device_model, const char *system_version, const char *app_version,
                            int use_secret_chats, const char *database_directory, const char *files_directory);

void td_json_client_destroy(void *client);

void td_json_client_write(void *client, const char *data);

const char *td_json_client_read(void *client, double timeout);

void td_json_client_wake_up(void *client);

#ifdef __cplusplus
}  // extern "C"
#endif
